<?php

namespace Drupal\views_data_export_phpspreadsheet\Hook;

use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Hook implementations for module views Data Export Phpspreadsheet.
 */
class ViewsDataExportPhpspreadsheet {

  use StringTranslationTrait;

  /**
   * Implements hook_help().
   */
  #[Hook('help')]
  public function help($route_name, RouteMatchInterface $route_match) {
    switch ($route_name) {
      // Main module help for the views export xlxs.
      case 'help.page.views_data_export_phpspreadsheet':
        return '<p>' . t('This module provides formats xlxs for views: For a full description of the module visit: <a href="https://www.drupal.org/project/views_data_export_phpspreadsheet">Data export phpspreadsheet</a>');

      default:
        break;

    }
  }

}
